const { defineConfig } = require("cypress");

module.exports = defineConfig({
  defaultCommandTimeout: 5000,
  video: false,
  screenshotOnRunFailure: false,
  chromeWebSecurity: false,
  retries: 0,
  viewportWidth: 1920,
  viewportHeight: 1080,
  e2e: {
    setupNodeEvents(on, config) {
      if(config.env.temp) {
        return {
          baseUrl: "https://portal-review-wppodc-1788.r.test.lemonpi.io/r",
          env: {
            env: "temp",
            manageUrl: "https://manage-review-wppodc-1788.r.test.lemonpi.io",
            measureUrl: "https://measure-review-wppodc-1788.r.test.lemonpi.io",
            restLoginUrl: "https://api-review-wppodc-1788.r.test.lemonpi.io/auth/user-token",
            apiGenerateToken: "https://api-review-wppodc-1788.r.test.lemonpi.io/auth",
            login: "/login",
            forgot_password: "/password-reset"
          }
        }
      } else if (config.env.test) {
        return {
          baseUrl: "https://portal.test.lemonpi.io/r/",
          env: {
            env: "test",
            manageUrl: "https://manage.test.lemonpi.io",
            measureUrl: "https://measure.test.lemonpi.io",
            restLoginUrl: "https://api.test.lemonpi.io/auth/user-token",
            login: "/login",
            forgot_password: "/password-reset"
          }
        }
      }
      },

    agencyUrl: "/r/{agency}",
    advertiserUrl: "/advertiser/{advertiser}",
    campaignUrl: "/campaign/{campaign}",
    adsetUrl: "/adset/{adset}",
  }
});
