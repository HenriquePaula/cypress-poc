export const generateToken = (username, password) => {
    cy
        .request({
            method: 'POST',
            url: `$(Cypress.env('apiGenerateToken')/user-token`,
            body: {
                "email": username,
                "password": password
            }
        }).then((response) => {
            expect(response.status).to.eq(200)
        expect(response.body).to.have.property('auth-token')
        cy.log('response token:', response.body['auth-token'])
        window.localStorage.setItem('token', response.body['auth-token'])
        cy.setCookie('token', response.body.token)
    })
}