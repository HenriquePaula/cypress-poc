export class NavigateTo {
    login() {
        cy.visit(Cypress.env('login'))
    }

    forgot_password() {
        cy.visit(Cypress.env('forgot_password'))
    }
}

export const navigateTo = new NavigateTo()