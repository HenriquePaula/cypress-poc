import {navigateTo} from '../../support/page_objetcs/navigateTo'

describe('Navigation tests', () => {

    it('Navigation test - Login page', () => {
        cy.log('Default environment:', Cypress.env('env'))
        navigateTo.login()
        cy.url().should('include', '/login')
        cy.get('[data-testid="email"]').as('emailInput').should('be.visible')
        cy.get('[data-testid="password"]').as('passwordInput').should('be.visible')
        cy.get('[data-testid="submit"]').as('loginButton').should('be.visible')
        cy.get('[data-testid="forgot-password"]').as('forgotPasswordButton').should('be.visible')

    })

    it('Navigation test - Forgot Password', () => {
        cy.log('Default environment:', Cypress.env('env'))
        navigateTo.forgot_password()
        cy.url().should('include', '/password-reset')
        cy.get('[data-testid="back-to-login"]').should('be.visible')
    })
})