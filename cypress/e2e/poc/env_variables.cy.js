describe('Testing some environment variables and configuration', () => {

    it('Checking variables from CLI: Environment and Temp_env', () => {
        // Default environment
        cy.log('Default environment:', Cypress.env('env'))
        expect(Cypress.env('env')).to.eql('temp')

        cy.log('Default environment temp_env:', Cypress.env('temp_env'))
        expect(Cypress.env('temp_env')).to.eql('WPPODC-1788')
    })

    it('Checking variables from cypress.env.json: Users', () => {
        cy.log("Temp default user:", Cypress.env('defaultUser')['temp'].email)
        expect(Cypress.env('defaultUser')['temp'].email).to.eql('admin@lemonpi.io')

        cy.log("Test default user:", Cypress.env('defaultUser')['test'].email)
        expect(Cypress.env('defaultUser')['test'].email).to.eql('e2e-acceptance@lemonpi.io')
    })

    it('Checking variables from cypress.config.js: URLs', () => {
        cy.log('Environment:', Cypress.env('env'))
        expect(Cypress.env('env')).to.eql('temp')

        cy.log('baseUrl:', Cypress.config('baseUrl'))
        expect(Cypress.config('baseUrl')).to.contains('/portal')

        cy.log('manageUrl:', Cypress.env('manageUrl'))
        expect(Cypress.env('manageUrl')).to.contains('/manage')

    })
})