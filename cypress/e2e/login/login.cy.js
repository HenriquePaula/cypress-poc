describe('Login test suite - Some tests on the login page', () => {

    beforeEach(() => {
        cy.visit('https://portal.test.lemonpi.io/')
        cy.url().should('include', '/login')
        cy.get('[data-testid="email"]').as('emailInput').should('be.visible')
        cy.get('[data-testid="password"]').as('passwordInput').should('be.visible')
        cy.get('[data-testid="submit"]').as('loginButton').should('be.visible')
        cy.get('[data-testid="forgot-password"]').as('forgotPasswordButton').should('be.visible')

    })

    it('Login with valid user and password and validate portal page is displayed', {id: 1, status: 'ready'}, () => {
        cy.get('@emailInput').type('e2e-acceptance@lemonpi.io')
        cy.get('@passwordInput').type('Ph357HcwzT-DZ:')
        cy.get('@loginButton').click()

        cy.get('[data-testid="assemble-navigate-button"]').as('assembleButton').should('be.visible')
        cy.get('[data-testid="manage-navigate-button"]').as('manageButton').should('be.visible')
        cy.get('[data-testid="measure-navigate-button"]').as('measureButton').should('be.visible')
    })

    it('Login with an invalid email and validate "Not a valid email address" error message', () => {
        cy.get('@emailInput').type('just_a_string')
        cy.get('@passwordInput').type('a')

        cy.get('.text-error').contains('Not a valid email address')
    })

    it('Login with an invalid password and validate "Invalid email/password combination" error message', () => {
        cy.get('@emailInput').type('e2e-acceptance@lemonpi.io')
        cy.get('@passwordInput').type('abc')
        cy.get('@loginButton').click()

        cy.get('[data-testid="login-error-message"]').contains('Invalid email/password combination.')
    })

    it('Login with no email and validate "The field should not be empty" error message', () => {
        cy.get('@passwordInput').type('abc')
        cy.get('@loginButton').click()

        cy.get('.text-error').contains('The field should not be empty')
    })
})