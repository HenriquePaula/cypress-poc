describe('Portal test suite - Testing the backend login', () => {

    it.skip('Logs in programmatically without using the UI', function () {

        // programmatically log us in without needing the UI
        const myToken = cy.request({
            method: 'POST',
            url: loginUrl,
            body: {email: username, password: password}
        })
            .then((response) => {
                return new Promise(resolve => {
                    expect(response.status).to.eq(200)
                    expect(response.body).to.have.property('auth-token')
                    const token = response.body['auth-token']
                    resolve(token)
                })
            })

        // now that we're logged in, we can visit any kind of restricted route!
        cy.log('My Token:', myToken)


        cy.visit('https://portal.test.lemonpi.io/')

        // cy.log('response:', resp)
    })

    it.skip('Authenticate and get the token', () => {

        // programmatically log us in without needing the UI
        cy
            .request(
                'POST',
                loginUrl,
                { email: username, password: password })
            .then(response => {
                expect(response.status).to.eq(200)
                expect(response.body).to.have.property('auth-token')
                cy.wrap(response.body['auth-token']).as('token')

        })

        // now that we're logged in, we can visit any kind of restricted route!
        cy.get('@token').then(token => {
            cy.log('what is this?')
        })
    })

    it.skip('Authenticate and get the token from a custom command', () => {

        cy.postToken(username, password)
        cy.log('Local storage token:', window.localStorage.getItem('token'))

    })

    it('Login using UI', () => {
        cy.login(
            Cypress.env('defaultUser')[Cypress.env('env')].email,
            Cypress.env('defaultUser')[Cypress.env('env')].password,
        )

        // Sidebar
        cy.get('[data-testid="Support"]').should('be.visible')
        cy.get('[data-testid="Docs"]').should('be.visible')
        cy.get('[data-testid="My Account"]').should('be.visible')
        cy.get('[data-testid="Logout"]').should('be.visible')

        // Portal
        cy.get('[data-testid="assemble-navigate-button"]').should('be.visible')
        cy.get('[data-testid="manage-navigate-button"]').should('be.visible')
        cy.get('[data-testid="measure-navigate-button"]').should('be.visible')
    })
})